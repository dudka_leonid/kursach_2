using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using TimetableCompiler.Logic;

namespace TimetableCompiler.OtherWindows
{
    public partial class TimetableReportW : Window
    {
        private StringBuilder text;
        public TimetableReportW(Timetable timetable)
        {
            InitializeComponent();
            text = new StringBuilder();
            foreach (var groupName in timetable.GroupNames)
            {
                List<Event> events = timetable.Events.FindAll(item => item.GroupName == groupName);
                text.Append("==============" + groupName + "==============" + "\n\n");
                foreach (var @event in events)
                {
                    text.Append(@event + "\n\n");
                }
                text.Append("============================" + "\n\n");
            }
            ReportTextBlock.Text = text.ToString();
        }
        private void Close_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void PrintToFile_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
            WorkFile workFile = new WorkFile(new WorkFile().ReportPath);
            workFile.SaveReport(text.ToString());
            MessageBox.Show("Файл записан по пути:\n"+workFile.ReportPath);
            }
            catch (FileNotFoundException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
