using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TimetableCompiler.Logic;

namespace TimetableCompiler.OtherWindows
{
    public partial class NewEvent : Window
    {
        private readonly Timetable _timetable;

        private Timetable _searchTimetable;
        public NewEvent(Timetable timetable)
        {
            _timetable = timetable;
            InitializeComponent();
            LoadDataComboBox();
        }
        public Event Event { get; private set; }
        /// <summary>
        ///     Создает новое событие (Event)
        /// </summary>
        private void CreateEvent_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var nameEvent = NameEventTextBox.Text;
                var audience = Convert.ToInt32(GetValue(AudienceTextBox, AudienceComboBox));
                var groupName = GetValue(GroupNameTextBox, GroupNameComboBox);
                var teacherName = GetValue(TeacherNameTextBox, TeacherNameComboBox);
                var date = (Event.DaysOfWeek) DaysOfWeekComboBox.SelectedIndex;
                var time = new Time(Convert.ToInt32(HoursComboBox.Text),
                    Convert.ToInt32(MinutesComboBox.Text));
                var details = DetailsTextBox.Text;
                if (!_timetable.Audiences.Contains(audience)) _timetable.Audiences.Add(audience);
                if (!_timetable.GroupNames.Contains(groupName)) _timetable.GroupNames.Add(groupName);
                if (!_timetable.TeacherNames.Contains(teacherName)) _timetable.TeacherNames.Add(teacherName);
                Event = new Event(nameEvent, audience, groupName, teacherName,
                    _timetable.Type, date, details, time);
                if (IsCheck())
                {
                    DialogResult = true;
                }
                else
                {
                    MessageBox.Show("No");
                }
            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        private bool IsCheck()
        {
            foreach (var timetableEvent in _timetable.Events)
            {
                if (timetableEvent.GroupName == Event.GroupName)
                {
                    if (timetableEvent.DayOfWeek == (Event.DaysOfWeek) DaysOfWeekComboBox.SelectedIndex)
                    {
                        if (timetableEvent.StartTime.Tick >= Event.StartTime.Tick
                            && timetableEvent.StartTime.Tick <= Event.EndTime.Tick)
                        {
                            return false;
                        }

                        if (timetableEvent.StartTime.Tick <= Event.StartTime.Tick
                            && timetableEvent.EndTime.Tick >= Event.EndTime.Tick)
                        {
                            return false;
                        }
                        if (timetableEvent.EndTime.Tick >= Event.StartTime.Tick 
                            && timetableEvent.EndTime.Tick <= Event.EndTime.Tick)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        private string GetValue(TextBox textBox, ComboBox comboBox)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(textBox.Text)) return textBox.Text;

                if (comboBox.SelectedIndex > -1) return comboBox.Text;

                MessageBox.Show(new NullReferenceException().Message);
            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message);
            }
            return null;
        }
        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
        // Заполнение элементов интерфейса данными 
        private void LoadDataComboBox()
        {
            GroupNameComboBox.ItemsSource = _timetable.GroupNames;
            AudienceTextBox.IsReadOnly = true;
            TeacherNameTextBox.IsReadOnly = true;
        }
        private void DaysOfWeekComboBox_OnDropDownOpened(object sender, EventArgs e)
        {
            List<string> daysOfWeek = Enum.GetNames(typeof(Event.DaysOfWeek)).ToList();
            if (GroupNameComboBox.SelectedIndex > -1 || GroupNameTextBox.Text.Length > 0)
            {
                int[] i = new int[7];
                _searchTimetable = new Timetable(_timetable.Name,_timetable.Type);
                _searchTimetable.Events.AddRange(_timetable.Events.FindAll(item => 
                    item.GroupName.Equals((string) GroupNameComboBox.SelectedItem)
                    || item.GroupName.Equals(GroupNameTextBox.Text)));
                foreach (var timetableEvent in _searchTimetable.Events)
                {
                        switch (timetableEvent.DayOfWeek)
                        {
                            case Event.DaysOfWeek.Понедельник:
                                i[0]+=1;
                                break;
                            case Event.DaysOfWeek.Вторник:
                                i[1]+=1;
                                break;
                            case Event.DaysOfWeek.Среда:
                                i[2]+=1;
                                break;
                            case Event.DaysOfWeek.Четверг:
                                i[3]+=1;
                                break;
                            case Event.DaysOfWeek.Пятница:
                                i[4]+=1;
                                break;
                            case Event.DaysOfWeek.Суббота:
                                i[5]+=1;
                                break;
                            case Event.DaysOfWeek.Воскресенье:
                                i[6]+=1;
                                break;
                    }
                }
                for (int j = 0; j < i.Length; j++)
                {
                    if (i[j] >= 14)
                    {
                        daysOfWeek.RemoveAt(j);
                    }
                }
                DaysOfWeekComboBox.ItemsSource = daysOfWeek;
            }
            else
            {
                MessageBox.Show("Введите вашу группу");
            }
        }
        private void HoursComboBox_OnDropDownOpened(object sender, EventArgs e)
        {
            if (DaysOfWeekComboBox.SelectedIndex > -1)
            {
                HoursComboBox.ItemsSource = Enumerable.Range(8, 11).ToList();
            }
            else
            {
                MessageBox.Show("Введите день недели");
            }
        }
        private void MinutesComboBox_OnDropDownOpened(object sender, EventArgs e)
        {
            if (HoursComboBox.SelectedIndex > -1)
            {
                MinutesComboBox.ItemsSource = Enumerable.Range(0, 60).ToList();
            }
            else
            {
                MessageBox.Show("Введите час занятий");
            }
        }
В        private void ComboBoxByData_OnDropDownOpened(object sender, EventArgs e)
        {
            if (MinutesComboBox.SelectedIndex > -1)
            {
                Time startTime = new Time(Convert.ToInt32(HoursComboBox.Text),
                                     Convert.ToInt32(MinutesComboBox.Text));
                Time endTime = new Time(startTime.Hour, startTime.Minute+40);
                List<int> audiences = _timetable.Audiences.ToList();
                List<string> teacherName = _timetable.TeacherNames.ToList();
                foreach (var timetableEvent in _timetable.Events)
                {
                    if (timetableEvent.DayOfWeek == (Event.DaysOfWeek) DaysOfWeekComboBox.SelectedIndex)
                    {
                        if (startTime.Tick >= timetableEvent.StartTime.Tick
                            && startTime.Tick <= timetableEvent.EndTime.Tick)
                        {
                            audiences.Remove(timetableEvent.Audience);
                            teacherName.Remove(timetableEvent.TeacherName);
                        }

                        if (startTime.Tick <= timetableEvent.StartTime.Tick
                            && endTime.Tick >= timetableEvent.EndTime.Tick)
                        {
                            audiences.Remove(timetableEvent.Audience);
                            teacherName.Remove(timetableEvent.TeacherName);
                        }
                        if (endTime.Tick >= timetableEvent.StartTime.Tick 
                            && endTime.Tick <= timetableEvent.EndTime.Tick)
                        {
                            audiences.Remove(timetableEvent.Audience);
                            teacherName.Remove(timetableEvent.TeacherName);
                        }
                    }
                }
                TeacherNameComboBox.ItemsSource = teacherName;
                AudienceComboBox.ItemsSource = audiences;
            }
            else
            {
                MessageBox.Show("Введите дату (все поля)");
            }
        }
        private void UIElement_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (MinutesComboBox.SelectedIndex > -1)
            {
                AudienceTextBox.IsReadOnly = false;
                TeacherNameTextBox.IsReadOnly = false;
            }
            else
            {
                MessageBox.Show("Введите дату (все поля)");
            }
        }
        private void GroupNameTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            GroupNameComboBox.SelectedIndex = -1;
            DaysOfWeekComboBox.SelectedIndex = -1;
            HoursComboBox.SelectedIndex = -1;
            MinutesComboBox.SelectedIndex = -1;
            TeacherNameComboBox.SelectedIndex = -1;
            AudienceComboBox.SelectedIndex = -1;
            TeacherNameTextBox.Clear();
            AudienceTextBox.Clear();
        }
        private void GroupNameComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GroupNameTextBox.Clear();
            DaysOfWeekComboBox.SelectedIndex = -1;
            HoursComboBox.SelectedIndex = -1;
            MinutesComboBox.SelectedIndex = -1;
            TeacherNameComboBox.SelectedIndex = -1;
            AudienceComboBox.SelectedIndex = -1;
            TeacherNameTextBox.Clear();
            AudienceTextBox.Clear();
        }
        private void DaysOfWeekComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HoursComboBox.SelectedIndex = -1;
            MinutesComboBox.SelectedIndex = -1;
            TeacherNameComboBox.SelectedIndex = -1;
            AudienceComboBox.SelectedIndex = -1;
            TeacherNameTextBox.Clear();
            AudienceTextBox.Clear();
        }
        private void HoursComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MinutesComboBox.SelectedIndex = -1;
            TeacherNameComboBox.SelectedIndex = -1;
            AudienceComboBox.SelectedIndex = -1;
            TeacherNameTextBox.Clear();
            AudienceTextBox.Clear();
        }
        private void MinutesComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TeacherNameComboBox.SelectedIndex = -1;
            AudienceComboBox.SelectedIndex = -1;
            TeacherNameTextBox.Clear();
            AudienceTextBox.Clear();
        }
        private void TeacherNameTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            TeacherNameComboBox.SelectedIndex = -1;
        }
        private void AudienceTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            AudienceComboBox.SelectedIndex = -1;
        }
        private void TeacherNameComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TeacherNameTextBox.Clear();
        }
        private void AudienceComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AudienceTextBox.Clear();
        }
    }
}