using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TimetableCompiler.Logic;

namespace TimetableCompiler.OtherWindows
{
    public partial class TimetableWindow : Window
    {
        private readonly Timetable _timetable;
        private readonly WorkFile _workFile;
        private List<TextBlock> _eventsCard ;
        public TimetableWindow(string filePath)
        {
            InitializeComponent();
            _workFile = new WorkFile(filePath);
            _eventsCard = new List<TextBlock>();
            try
            {
                _timetable = _workFile.LoadTimetable();
            }
            catch (FileLoadException exception)
            {
                MessageBox.Show(exception.Message);
            }
            
            MessageBox.Show(filePath + "\n" + _timetable.Events.Count);
            Title = filePath + " - " + _timetable.Type;
        }
        /// <summary>
        /// Загружает Events на экран
        /// </summary>
        private void PrintEvents(string groupName)
        {
            List<Event> events = _timetable.Events.FindAll(item => item.GroupName.Equals(groupName));
            MessageBox.Show(events.Count.ToString()+"\n"+groupName);
            foreach (var timetableEvent in events) PrintCard(timetableEvent, Brushes.CadetBlue);
        }
        private void GroupNameComboBox_OnDropDownOpened(object sender, EventArgs e)
        {
            GroupNameComboBox.ItemsSource = _timetable.GroupNames.ToList();  
        }
        private void GroupNameComboBox_OnDropDownClosed(object sender, EventArgs e)
        {
            if (GroupNameComboBox.SelectedIndex > -1)
            {
                foreach (var textBlock in _eventsCard)
                {
                    EventsGrid.Children.Remove(textBlock);
                }
                _eventsCard.Clear();
                PrintEvents(GroupNameComboBox.Text);
            }
        }
        /// <summary>
        /// Выводит информацию о Event ввиде Card на экран
        /// </summary>
        /// <param name="eventTimetable">Event</param>
        /// <param name="color">Color</param>
        private void PrintCard(Event eventTimetable, SolidColorBrush color)
        {
                var text = eventTimetable.ToString();
                var textBlock = new TextBlock();
                textBlock.Text = eventTimetable.Name;
                textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                textBlock.Padding = new Thickness(2);
                textBlock.HorizontalAlignment = HorizontalAlignment.Stretch;
                textBlock.VerticalAlignment = VerticalAlignment.Stretch;
                textBlock.Background = color;
                textBlock.Margin = new Thickness(2, eventTimetable.StartTime.Minute, 2,
                    60 - eventTimetable.EndTime.Minute);
                var contextMenu = new ContextMenu();
                var viewItem = new MenuItem();
                viewItem.Click += (sender, args) => { ShowDetails(text); };
                viewItem.Header = "Просмотр";
                contextMenu.Items.Add(viewItem);
                var deleteItem = new MenuItem();
                deleteItem.Header = "Удалить";
                deleteItem.Click += (sender, args) => { DeleteEventCard(eventTimetable, textBlock); };
                contextMenu.Items.Add(deleteItem);
                textBlock.ContextMenu = contextMenu;
                EventsGrid.Children.Add(textBlock);
                Grid.SetColumn(textBlock, (int) eventTimetable.DayOfWeek * 2 + 2);
                Grid.SetRow(textBlock, (eventTimetable.StartTime.Hour - 8) * 2);
                Grid.SetRowSpan(textBlock, eventTimetable.EndTime.Hour - eventTimetable.StartTime.Hour + 2);
                _eventsCard.Add(textBlock);
        }
        /// <summary>
        /// Удаляет Event с экрана и из списка
        /// </summary>
        /// <param name="eventTimetable">Удаляемый элемент с экрана</param>
        /// <param name="textBlock">Удаляемый event из списка, соответвующий элементу с экрана</param>
        private void DeleteEventCard(Event eventTimetable, TextBlock textBlock)
        {
            EventsGrid.Children.Remove(textBlock);
            _timetable.RemoveEvent(eventTimetable);
        }
        /// <summary>
        /// Отображает text
        /// </summary>
        /// <param name="text">text</param>
        private void ShowDetails(string text)
        {
            MessageBox.Show(text);
        }
        /// <summary>
        /// Создание нового Event
        /// </summary>
        private void CreateEventMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            var newEvent = new NewEvent(_timetable);
            if (newEvent.ShowDialog() == true)
            {
                _timetable.AddEvent(newEvent.Event);
                _timetable.Sort();
                MessageBox.Show("Событие создано!");
                PrintCard(newEvent.Event, Brushes.CadetBlue);
            }
        }
        /// <summary>
        /// Открытие окна отчета
        /// </summary>
        private void OpenReportListsMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            TimetableReportW timetableReportW = new TimetableReportW(_timetable);
            timetableReportW.Show();
        }
        /// <summary>
        /// Сохранение всех данных
        /// </summary>
        private void SaveData_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _workFile.SaveTimetable(_timetable);
                MessageBox.Show("Данные сохранены");
            }
            catch (DirectoryNotFoundException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        private void OpenCreateTimetableWindowMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var newTimetable = new NewTimetable();
                if (newTimetable.ShowDialog() == true)
                {
                    WorkFile workFile = new WorkFile();
                    var timetable = new TimetableWindow(workFile.FilePath +
                                                        newTimetable.NameTimetableTextBox.Text);
                    timetable.Show();
                }
            }
            catch (FileLoadException exception)
            {
                MessageBox.Show(exception.Message + '\n' + exception);
            }
        }
        private void OpenMainWindowMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }
        private void DeleteMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            var acceptWindow = new AcceptWindow();
            if (acceptWindow.ShowDialog() == true)
            {
                try
                {
                    MainWindow mainWindow = new MainWindow();
                    File.Delete(_workFile.FilePath);
                    mainWindow.Show();
                    Close();
                }
                catch (FileLoadException exception)
                {
                    MessageBox.Show(exception.Message + '\n' + exception);
                }
            }
        }
    }
}