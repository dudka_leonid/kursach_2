using System;
using System.Linq;
using System.Windows;
using TimetableCompiler.Logic;

namespace TimetableCompiler.OtherWindows
{
    public partial class NewTimetable : Window
    {
        public NewTimetable()
        {
            InitializeComponent();

            var types = Enum.GetNames(typeof(Event.Types));
            foreach (var type in types) TypesTimetableComboBox.Items.Add(type);
        }
        private void CreateTimetable_OnClick(object sender, RoutedEventArgs e)
        {
            var type = (Event.Types) TypesTimetableComboBox.SelectedIndex;
            var timetable = new Timetable(NameTimetableTextBox.Text, type);
            var workFile = new WorkFile(new WorkFile().FilePath + timetable.Name);
            workFile.SaveTimetable(timetable);
            DialogResult = true;
        }
        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}