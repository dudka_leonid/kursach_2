﻿using System;
using System.IO;
using System.Windows;
using TimetableCompiler.Logic;
using TimetableCompiler.OtherWindows;

namespace TimetableCompiler
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly WorkFile _workFile = new WorkFile();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                var userSaveTimetable = _workFile.GetUserSaveTimetable();
                TimetablesListBox.Items.Clear();
                TimetablesListBox.ItemsSource = userSaveTimetable;
            }
            catch (FileLoadException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        /// <summary>
        ///     При выборе из контекстного меню, открывает нужное рассписание
        /// </summary>
        private void OpenTimetable_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var timetable = new TimetableWindow(_workFile.FilePath + TimetablesListBox.SelectedItem);
                timetable.Show();
                Close();
            }
            catch (FileLoadException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        /// <summary>
        ///     При выборе из контекстного меню, удаляет нужное рассписание
        /// </summary>
        private void DeleteTimetable_OnClick(object sender, RoutedEventArgs e)
        {
            var acceptWindow = new AcceptWindow();
            if (acceptWindow.ShowDialog() == true)
            {
                try
                {
                    var timetable = TimetablesListBox.SelectedItem;
                    File.Delete(_workFile.FilePath + timetable);
                    TimetablesListBox.Items.Remove(timetable);
                }
                catch (FileNotFoundException exception)
                {
                    
                    MessageBox.Show(exception.Message);
                }
            }
        }
        /// <summary>
        ///     открывает окно создания расписания
        /// </summary>
        private void OpenCreateTimetableWindow_OnClick(object sender, RoutedEventArgs e)
        {
            var newTimetable = new NewTimetable();
            if (newTimetable.ShowDialog() == true)
            {
                try
                {
                    var timetable = new TimetableWindow(_workFile.FilePath +
                                                        newTimetable.NameTimetableTextBox.Text);
                    timetable.Show();
                    Close();
                }
                catch (FileLoadException exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }
    }
}