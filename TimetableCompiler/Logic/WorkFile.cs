using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace TimetableCompiler.Logic
{
    public class WorkFile
    {
        private readonly BinaryFormatter _formatter = new BinaryFormatter();
        // Основная папка записей пользователя
        private const string MainDirPath = "C:\\WindowsAppDir\\UserSave\\";
        // Папка с отчетом
        private const string ReportDirPath = "C:\\WindowsAppDir\\Reports\\"; 
        public WorkFile() : this(MainDirPath){}

        public WorkFile(string filePath)
        {
            FilePath = filePath;
            Directory.CreateDirectory(MainDirPath);
            Directory.CreateDirectory(ReportDirPath);
        }
        public string FilePath { get; set; }
        public string ReportPath
        {
            get { return ReportDirPath; }
        }
        /// <summary>
        /// Получить список файлов пользователя
        /// </summary>
        /// <returns>список имён файлов пользователя</returns>
        public List<string> GetUserSaveTimetable()
        {
            var userSaveTimetable = new List<string>();
            var dir = new DirectoryInfo(FilePath);
            foreach (var file in dir.GetFiles()) userSaveTimetable.Add(file.Name);
            return userSaveTimetable;
        }
        /// <summary>
        /// Загрузить таблицу
        /// </summary>
        /// <returns>таблица</returns>
        public Timetable LoadTimetable()
        {
            var fs = new FileStream(FilePath,
                FileMode.OpenOrCreate);
            var timetable = (Timetable) _formatter.Deserialize(fs);
            fs.Close();
            return timetable;
        }
        /// <summary>
        /// Сохранить таблицу
        /// </summary>
        public bool SaveTimetable(Timetable timetable)
        {
            var fs = new FileStream(FilePath,
                FileMode.OpenOrCreate);
            _formatter.Serialize(fs, timetable);
            fs.Close();
            return true;
        }
        /// <summary>
        /// Удалить таблицу
        /// </summary>
        public bool DeleteTimetable()
        {
            File.Delete(FilePath);
            return true;
        }
        /// <summary>
        /// Записать в файл таблицу
        /// </summary>
        public bool SaveReport(string report)
        {
            File.WriteAllText(ReportPath+"Report.txt",report, Encoding.UTF8);
            return true;
        }
    }
}