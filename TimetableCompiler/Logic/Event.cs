using System;
using System.Text;

namespace TimetableCompiler.Logic
{
    [Serializable]
    public class Event
    {
        public enum DaysOfWeek
        {
            Понедельник,
            Вторник,
            Среда,
            Четверг,
            Пятница,
            Суббота,
            Воскресенье
        }
        public enum Types
        {
            School
        }
        public Event(string name, int audience, string groupName,
            string teacherName, Types type, DaysOfWeek dayOfWeek, string details, Time startTime)
        {
            Name = name;
            Audience = audience;
            GroupName = groupName;
            TeacherName = teacherName;
            Type = type;
            DayOfWeek = dayOfWeek;
            Details = details;
            StartTime = startTime;
        }
        public string Name { get; set; }
        public int Audience { get; set; }
        public string GroupName { get; set; }
        public string TeacherName { get; set; }
        public Types Type { get; set; }
        public DaysOfWeek DayOfWeek { get; set; }
        public string Details { get; set; }
        public Time StartTime { get; set; }
        public Time EndTime
        {
            get
            {
                var endTime = new Time(0, 0);
                if (Type == Types.School)
                {
                    endTime = new Time(StartTime.Hour, StartTime.Minute + 40);
                    return endTime;
                }
                return endTime;
            }
        }
        public override string ToString()
        {
            var text = new StringBuilder();

            text.Append("Название: " + Name + '\n')
                .Append("Аудитория: " + Audience + '\n')
                .Append("Название группы" + GroupName + '\n')
                .Append("Имя преподавателя: " + TeacherName + '\n')
                .Append("Время: " + StartTime.Hour + ':' + StartTime.Minute)
                .Append(" - " + EndTime.Hour + ':' + EndTime.Minute);
            return text.ToString();
        }
    }
    [Serializable]
    public class Time
    {
        private int _hour;
        private int _minute;
        public Time(int hour, int minute)
        {
            Hour = hour;
            Minute = minute;
        }
        public int Tick
        {
            get { return _hour * 60 + _minute; }
        }
        public int Hour
        {
            get { return _hour; }
            set
            {
                if (value < 24)
                    _hour = value;
                else
                    throw new ArgumentOutOfRangeException();
            }
        }
        public int Minute
        {
            get { return _minute; }
            set
            {
                if (value > 0)
                {
                    _minute = value % 60;
                    Hour += value / 60;
                }
            }
        }
    }
}