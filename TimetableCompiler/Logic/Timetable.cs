using System;
using System.Collections.Generic;

namespace TimetableCompiler.Logic
{
    [Serializable]
    public class Timetable
    {
        private readonly string _name;
        private readonly List<Event> _events;
        private readonly List<int> _audiences;
        private readonly List<string> _groupNames;
        private readonly List<string> _teacherNames;
        private readonly Event.Types _type;
        public Timetable(string name, Event.Types type)
        {
            _name = name;
            _events = new List<Event>();
            _audiences = new List<int>();
            _groupNames = new List<string>();
            _teacherNames = new List<string>();
            _type = type;
        }
        public string Name
        {
            get { return _name; }
        }
        public List<Event> Events 
        {
            get { return _events; }
        }
        public List<int> Audiences 
        {
            get { return _audiences; }
        }
        public List<string> GroupNames 
        {
            get { return _groupNames; }
        }
        public List<string> TeacherNames 
        {
            get { return _teacherNames; }
        }
        public Event.Types Type 
        {
            get { return _type; }
        }
        public void AddEvent(Event newEvent)
        {
            Events.Add(newEvent);
        }
        public void RemoveEvent(Event newEvent)
        {
            Events.Remove(newEvent);
        }
        public void Sort()
        {
            Events.Sort((firstEvent, secondEvent) => 
                                    firstEvent.StartTime.Tick.CompareTo(secondEvent.StartTime.Tick));
        }
    }
}